# Роль устанавливает последнюю версию Grafana Promtail с GitHub.

Используются следующие параметры

---
promtail_user: promtail
promtail_group: promtail
promtail_conf_dir: /etc/promtail
promtail_tmp_dir: /tmp
promtail_bin: /usr/local/bin/promtail
promtail_bind_port: 9080
promtail_loki_url: http://localhost:3100/loki/api/v1/push
